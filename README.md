# Mattermost Random Team

*Mattermost Random Team* is a bot written in Python that interacts with the [Mattermost API](https://api.mattermost.com/) using [mattermostdriver](https://github.com/Vaelor/python-mattermost-driver) and forms small private channels from users in your Mattermost team on a regular basis to help them meet people they otherwise would not.

Differences to other similar approaches are:
 - Small group instead of one-to-one is less stressful for less extrovert users
 - The "Random Team" is not just a coffee break - it can exist for a week or month
 - The discussion is not bound to specific time, users can write when they have time

### Features

- Assigns users randomly into private teams
- Configurable team size
- Typically to be run as a cron job
- Users join the random teaming by entering a special public channel
- Users exit the random teaming by leaving their current random team
- Random team channels are private for their users and conversation history is not visible across randomization iterations
- Discussion revival feature: The bot posts to the channels with given schedule if there has not been discussion lately
- Support for generating an introductory message regarding each random team member by calling a custom external command when a new channel is formed
- Statistics print command for monitoring how Random Teams is being used in your organization
- If user does not post anything during a team period, they are considered "passive". On the next round, teams are formed from active members and these passive members are appended to those teams as extra members. This is done in order to keep at least minimum amount of active members in each team.

### Future Features?

- Ice breaker discussion topics by the bot

### Technologies Used

- Python 3.5+
- [Mattermost API](https://api.mattermost.com/) via Mattermostdriver
- Docker
- Supercronic

### User instructions

- Join the random teams public channel created by your server administrator
- Wait for the next randomization (e.g. weekend)
- Find yourself on a new channel with a few random people 
- Enjoy chatting with them and getting to know each other until the next randomization
- If you feel this is not your cup of tea, leave the random channel. You won't be assigned automatically in the next randomization.

### Deployment

Since the app is not more complicated than a python script, you can deploy manually as desired. A docker image build is supported for more organized infrastructure. 

Since Mattermost API does not provide permanent channel deletion, the random teams app will rename abandoned channels with a static prefix and a random postfix. To delete the channels permanently, setup a cron task on the mattermost server to call the following command: 
`mattermost channel list <teamname> | grep "to-be-deleted" | cut -d ' ' -f 1 | while read line ; do echo Deleting ${line} ; mattermost channel delete <teamname>:${line} --confirm ; done`
 where 'to-be-deleted' is the configurable marker specified in the randomteam app environment.

Each deployment style requires
- Creating a bot account and access token https://docs.mattermost.com/developer/bot-accounts.html 
- Adding the bot to your Mattermost team
- Adding a public entry channel for Random Teams feature
- Adding the periodical cleanup routine described above to your mattermost installation
- Defining the environment variables for app configuration. An example .env file is found in the examples/ directory and shall be customized for the use case.

#### Docker deployment

1. Fetch the image: `docker pull lassiniemisto/mattermost-randomteam:latest`

2. For cron scheduled background job: `docker run --env-file path-to-yor-.env -d lassiniemisto/mattermost-randomteam` (remember to specify the cron schedule in the respective environment variable)

3. For one-shot immediate operation:  `docker run --env-file path-to-yor-.env -d lassiniemisto/mattermost-randomteam randomteam` (with `-r` for discussion revival)

#### Manual deployment without Docker

1. Clone the repository into a directory

2. Install Python 3.5+, `pip`, and `cron`.
    - On Ubuntu, this can be done by running `sudo apt-get update && apt-get install -y -qq cron python3 python3-pip`

3. Install the Python packages by running `sudo pip3 install -r requirements.txt` in the directory

4. Place the environment `.env` file in the cloned repository root

5. Run the script `randomteam.py` in desired schedule using cron or even a CI job (schedule discussion revival with separate cron schedule and -r command line switch)

#### Manually building the docker image

1. Clone the repository into a directory

2. Build the docker image `docker build . --tag mattermost-randomteam`

#### Using the statistics feature

Statistics will be logged to the Random Teams logfile, every time the randomization happens. To get statistics on-demand, run the script or docker command with `-s`. The following statistics are currently provided:
- channels_total
- members_total
- posts_total
- posts_per_channel_min
- posts_per_channel_max
- posts_per_channel_avg


### License

```
MIT License

Copyright 2020 Lassi Niemistö

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```

from mattermostdriver import Driver, exceptions
import random
import math
import uuid
import requests
import time
import sys
import subprocess
import shlex
import logging
from datetime import datetime
from randomteamutils import config


def main():
    logging.basicConfig(format = '%(asctime)s %(levelname)-8s %(message)s', level = logging.INFO, datefmt = '%Y-%m-%d %H:%M:%S')
     
    # Get connection and team id
    mm = get_mattermost_connection()
    team = mm.teams.get_team_by_name(config.TEAM_NAME)
    team_id = team['id']

    # Collect here all users onboard the randomization
    users_onboard = {}

    # Revive channels -mode
    if len(sys.argv) > 1:
        if sys.argv[1] == '-r':
            revive_channels(mm, team_id)
        elif sys.argv[1] == '-s':
            print_stats(mm, team_id)
        else:
            logging.info("Invalid command line")
        return

    # First rip the existing users from current random teams to get the users' posting history
    while True:
        try:
            users_onboard.update(rip_existing_random_channel_users(mm, team_id))
            break
        except requests.exceptions.ConnectionError as e:
            logging.info("Connection error during rip_existing_random_channel_users, retrying after 10s")
            time.sleep(10)
            pass

    # Then add new users from entry channel (may be overlapping)
    while True:
        try:
            users_onboard.update(rip_entry_channel_users(mm, team_id))
            break
        except requests.exceptions.ConnectionError as e:
            logging.info("Connection error during rip_entry_channel_users, retrying after 10s")
            time.sleep(10)
            pass

    # Remove deactivated users
    logging.info("Filtering out deactivated users")
    users_onboard = filter_active_users(users_onboard)

    # Categorize users based on their previous activity
    logging.info("Detecting passive users")
    active_users, passive_users = split_users_to_active_and_passive(users_onboard)

    # Re-randomize the teams based on the active users
    logging.info("Creating new random teams from active users")
    random_teams = generate_random_teams(active_users)

    # Evenly divide the passive users to the random teams
    logging.info("Appending passive users to random teams")
    while len(passive_users) > 0:
        for rt in random_teams:
            if len(passive_users) > 0:
                next_key = next(iter(passive_users))
                logging.info("Appending passive user " + passive_users[next_key]['username'])
                rt.append(passive_users[next_key])
                passive_users.pop(next_key, None)
            else:
                break

    # Create new channels accordingly
    created_channels = []
    chan_i = 1
    for team_users in random_teams:
        intro_msgs = generate_introductory_messages(team_users)
        while True:
            try:
                new_ch, chan_i = create_new_random_channel(mm, team_id, team_users, chan_i)
                chan_i += 1
                created_channels.append(tuple([new_ch, intro_msgs]))
                break
            except requests.exceptions.ConnectionError as e:
                logging.info("Connection error during create_new_random_channel, retrying after 10s")
                time.sleep(10)
                pass

    # Post welcome messages
    for chan, intro_msgs in created_channels:
        mm.posts.create_post(options={'channel_id': chan['id'], 'message': config.WELCOME_MESSAGE})
        # If user introduction messages were generated, post them to channel after the welcome message
        for msg in intro_msgs:
            mm.posts.create_post(options={'channel_id': chan['id'], 'message': msg})


def get_mattermost_connection():
    mm = Driver({
        # Authentication, see https://vaelor.github.io/python-mattermost-driver/#authentication
        'url': config.URL,
        'token': config.BOT_TOKEN,
        'auth': None,  # No custom Auth header creation class

        # Optional options
        'scheme': 'https',
        'port': config.PORT,
        'basepath': '/api/v4',
        'verify': config.VERIFY_CERT,
        'timeout': 30,
        'request_timeout': None,
        'debug': False
    })

    mm.login()
    return mm

# Collect new joiners from entry channel and remove them from there
def rip_entry_channel_users(mm, team_id):
    ripped_users = {}
    entry_ch = mm.channels.get_channel_by_name(team_id, config.ENTRY_CHANNEL_NAME)
    entry_ch_members = mm.channels.get_channel_members(entry_ch['id'], params={'per_page': '10000'})
    for u in entry_ch_members:
        entry_usr = mm.users.get_user(u['user_id'])
        entry_usr['previous_period_posts'] = 1 # Assume newcomers are active
        if not entry_usr['username'] in config.EXCLUDED_USERS:
            ripped_users[entry_usr['username']] = entry_usr
            mm.channels.remove_channel_member(entry_ch['id'], entry_usr['id'])
            logging.info("Ripping entry channel member " + entry_usr['username'])
    return ripped_users


# Rip users from existing channels and remove them from there
def rip_existing_random_channel_users(mm, team_id):
    ripped_users = {}
    existing_channels = get_existing_random_channels(mm, team_id)

    print_stats(mm, team_id)

    for old_ch in existing_channels:
        logging.info("Found old channel " + old_ch['name'])

        old_ch_members = mm.channels.get_channel_members(old_ch['id'], params=None)
        for u in old_ch_members:
            old_u = mm.users.get_user(u['user_id'])
            if not old_u['username'] in config.EXCLUDED_USERS:
                # Store the previous period posting activity as an extra field to the user object
                old_u['previous_period_posts'] = count_posts_per_user(mm, team_id, old_u['username'], old_ch['name'])
                ripped_users[old_u['username']] = old_u

                logging.info(" Ripping old channel member " + old_u['username'])

        # Rename the channel with a unique string with deletion marker
        while True:
            deleted_channel_name = config.TO_BE_DELETED_MARKER + "-" + str(uuid.uuid4().hex)
            try:
                mm.channels.patch_channel(old_ch['id'], options={'name': deleted_channel_name})
                break
            except:
                # Same uuid used earlier, retry
                pass

        # 'Delete' the channel to merely archive it and make it invisible, waiting for deletion using the cli tools
        mm.channels.delete_channel(old_ch['id'])

    return ripped_users


def get_existing_random_channels(mm, team_id):
    found_channels = []
    for ch_num in range(1, config.MAX_RANDOM_CHANNELS + 1):
        try:
            found_ch = mm.channels.get_channel_by_name(team_id, config.RANDOM_CHANNEL_BASE_NAME + str(ch_num))
            found_channels.append(found_ch)

        except exceptions.ResourceNotFound as ex:
            # channel not found
            pass
    return found_channels


# Get only non-deactivated users from the list
def filter_active_users(users_onboard):
    valid_users = {}
    for u in users_onboard.values():
        if int(u['delete_at']) == 0:
            valid_users[u['username']] = u
        else:
            logging.info("Filtering out deactivated user " + u['username'])
    return valid_users

# Categorize user list based on their activity during last period
def split_users_to_active_and_passive(users_onboard):
    active = {}
    passive = {}
    for u in users_onboard.values():
        # Allow max 50% passive users (team size grows max 2x)
        if u['previous_period_posts'] == 0 and len(passive) < (len(users_onboard) / 2):
            passive[u['username']] = u
        else:
            active[u['username']] = u

    return active, passive

# Distribute onboard users to new random channels
def generate_random_teams(users_onboard):
    random_teams = []
    channels_to_add = max(min(math.floor(len(users_onboard) / config.USERS_PER_CH), config.MAX_RANDOM_CHANNELS), 1)
    if len(users_onboard) > 0:
        users_per_ch_optimal = math.floor(len(users_onboard) / channels_to_add)
        orphan_users = len(users_onboard) % users_per_ch_optimal
        extra_users_max = math.ceil(orphan_users / channels_to_add)

        for chan_i in range(1, channels_to_add + 1):
            # Add the expected amount of users and necessary amount of extra users to mitigate uneven division
            team_users_list = []
            extra_users = 0
            if len(users_onboard) % users_per_ch_optimal > 0:
                extra_users = min(len(users_onboard) % users_per_ch_optimal, extra_users_max)

            while len(team_users_list) < (users_per_ch_optimal + extra_users):
                add_username = random.choice(list(users_onboard.keys()))
                team_users_list.append(users_onboard[add_username])
                users_onboard.pop(add_username)

            random_teams.append(team_users_list)

    return random_teams


# Create new channel
def create_new_random_channel(mm, team_id, team_users, chan_i):
    while chan_i <= config.MAX_RANDOM_CHANNELS:
        chan_name = config.RANDOM_CHANNEL_BASE_NAME + str(chan_i)
        logging.info("Creating " + chan_name)

        try:
            new_ch = mm.channels.create_channel(options={
                'team_id': team_id,
                'name': chan_name,
                'display_name': config.RANDOM_CHANNEL_BASE_DISP_NAME + ' ' + str(chan_i),
                'type': 'P'
            })
            break
        except exceptions.InvalidOrMissingParameters:
            chan_i += 1

    for u in team_users:
        mm.channels.add_user(new_ch['id'], options={'user_id': u['id']})
        logging.info(" Added users " + u['username'])

    return tuple([new_ch, chan_i])


# Try to revive discussion on those random channels, which have been silent for too long time
def revive_channels(mm, team_id):
    existing_channels = get_existing_random_channels(mm, team_id)
    unix_timestamp_now_ms = int(round(time.time() * 1000))
    search_posts_since = unix_timestamp_now_ms - (1000 * 60 * config.REVIVE_THRESHOLD_MINUTES)
    for ch in existing_channels:
        posts_since_limit = mm.posts.get_posts_for_channel(ch['id'], params={'since': search_posts_since})
        if len(posts_since_limit['posts']) == 0:
            logging.info("Reviving channel " + ch['name'])
            mm.posts.create_post(options={
                'channel_id': ch['id'],
                'message': config.REVIVE_STATIC_MSG})


def count_posts_per_user(mm, team_id, username, channelname):
    try:
        posts_by_usr = mm.posts.search_for_team_posts(team_id,
                                                      options={'terms': 'from:' + username + ' in:' + channelname,
                                                               'is_or_search': False,
                                                               'include_deleted_channels': False})
        return len(posts_by_usr['posts'])
    except:
        return 0


# Get statistics about the activity in Random Teams
def print_stats(mm, team_id):
    existing_channels = get_existing_random_channels(mm, team_id)

    total_members = 0
    total_posts = 0
    min_posts = 99999999
    max_posts = 0
    for ch in existing_channels:
        chan_posts = ch['total_msg_count']
        total_posts += chan_posts
        min_posts = min(min_posts, chan_posts)
        max_posts = max(max_posts, chan_posts)

        stats = mm.channels.get_channel_statistics(ch['id'])
        total_members += stats['member_count'] - 1 # Subtract bot user

    avg_posts = 0
    if len(existing_channels) > 0:
        avg_posts = total_posts / len(existing_channels)

    logging.info("statistics " + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    logging.info("channels_total " + str(len(existing_channels)))
    logging.info("members_total " + str(total_members))
    logging.info("posts_total " + str(total_posts))
    logging.info("posts_per_channel_max " + str(max_posts))
    logging.info("posts_per_channel_min " + str(min_posts))
    logging.info("posts_per_channel_avg " + str(avg_posts))


# Use external command to generate a introductory message about every team member
def generate_introductory_messages(users):
    ret = []
    if config.INTRODUCTORY_GENERATOR_CMD != '':
        for u in users:
            cmd_template = config.INTRODUCTORY_GENERATOR_CMD
            cmd_template = cmd_template.replace('%username%', u['username'])
            cmd_template = cmd_template.replace('%email%', u['email'])
            ret.append(subprocess.run(shlex.split(cmd_template), stdout=subprocess.PIPE).stdout.decode('utf-8'))
    return ret

if __name__ == '__main__':
    main()

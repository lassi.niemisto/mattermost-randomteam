import os
from dotenv import load_dotenv

__DIR__ = os.path.dirname(__file__)
load_dotenv(dotenv_path=__DIR__ + '/../.env')

URL = os.getenv('RANDOMTEAM_MATTERMOST_URL').lstrip('"').rstrip('"')
PORT = int(os.getenv('RANDOMTEAM_MATTERMOST_PORT'))
VERIFY_CERT = (os.getenv('RANDOMTEAM_MATTERMOST_VERIFY_CERT') == "TRUE")
BOT_USER = os.getenv('RANDOMTEAM_MATTERMOST_BOT_USER').lstrip('"').rstrip('"')
BOT_TOKEN = os.getenv('RANDOMTEAM_MATTERMOST_BOT_TOKEN').lstrip('"').rstrip('"')
TEAM_NAME = os.getenv('RANDOMTEAM_MATTERMOST_TEAM_NAME').lstrip('"').rstrip('"')
MAX_RANDOM_CHANNELS = int(os.getenv('RANDOMTEAM_MATTERMOST_MAX_RANDOM_CHANNELS'))
EXCLUDED_USERS = os.getenv('RANDOMTEAM_MATTERMOST_EXCLUDED_USERS').lstrip('"').rstrip('"').split(';')
EXCLUDED_USERS.append(BOT_USER)
USERS_PER_CH = int(os.getenv('RANDOMTEAM_MATTERMOST_USERS_PER_CH'))
RANDOM_CHANNEL_BASE_NAME = os.getenv('RANDOMTEAM_MATTERMOST_RANDOM_CHANNEL_BASE_NAME').lstrip('"').rstrip('"')
RANDOM_CHANNEL_BASE_DISP_NAME = os.getenv('RANDOMTEAM_MATTERMOST_RANDOM_CHANNEL_BASE_DISP_NAME').lstrip('"').rstrip('"')
ENTRY_CHANNEL_NAME = os.getenv('RANDOMTEAM_MATTERMOST_ENTRY_CHANNEL_NAME').lstrip('"').rstrip('"')
TO_BE_DELETED_MARKER = os.getenv('RANDOMTEAM_MATTERMOST_DELETED_CH_PREFIX').lstrip('"').rstrip('"')
WELCOME_MESSAGE = os.getenv('RANDOMTEAM_MATTERMOST_WELCOME_MESSAGE').lstrip('"').rstrip('"')
REVIVE_THRESHOLD_MINUTES = int(os.getenv('RANDOMTEAM_MATTERMOST_REVIVE_THRESHOLD_MINUTES').lstrip('"').rstrip('"'))
REVIVE_STATIC_MSG = os.getenv('RANDOMTEAM_MATTERMOST_REVIVE_STATIC_MSG').lstrip('"').rstrip('"')
INTRODUCTORY_GENERATOR_CMD = os.getenv('RANDOMTEAM_INTRODUCTORY_GENERATOR_CMD').lstrip('"').rstrip('"')



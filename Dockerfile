FROM ubuntu:20.04

# Install dependencies
RUN apt-get update && apt-get install -y -qq python3 python3-pip curl

# Install supercronic, a cron alternative which
#  - Is able to run as non root
#  - Preserves the env passed to container
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.9/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=5ddf8ea26b56d4a7ff6faecdd8966610d5cb9d85
RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# Copy over project files
RUN mkdir -p /opt/mattermost-randomteam
WORKDIR /opt/mattermost-randomteam
COPY . /opt/mattermost-randomteam

# Install Python libraries
RUN pip3 install -r requirements.txt

# Add a limited user
RUN groupadd -r limiteduser && useradd --no-log-init -r -g limiteduser limiteduser
RUN mkdir -p /home/limiteduser/ && chown limiteduser:limiteduser /home/limiteduser/
USER limiteduser

# Alias the randomizer command for easy one-shot runs
RUN echo 'alias randomteam="/usr/bin/python3 /root/mattermost-randomteam/randomteam.py"' >> /home/limiteduser/.bashrc

# Generate a crontab based on env variables and start supercronic
CMD echo "$RANDOMTEAM_CRON_SCHED /usr/bin/python3 /opt/mattermost-randomteam/randomteam.py >> /home/limiteduser/randomteam.log 2>&1" > /home/limiteduser/generated_crontab \
 && if [ ! -z "$RANDOMTEAM_REVIVE_CRON_SCHED" ]; then echo "$RANDOMTEAM_REVIVE_CRON_SCHED /usr/bin/python3 /opt/mattermost-randomteam/randomteam.py -r >> /home/limiteduser/randomteam.log 2>&1" >> /home/limiteduser/generated_crontab ; fi \
 && supercronic /home/limiteduser/generated_crontab
